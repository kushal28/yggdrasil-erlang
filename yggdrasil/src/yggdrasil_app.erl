%%%-------------------------------------------------------------------
%% @doc yggdrasil public API
%% @end
%%%-------------------------------------------------------------------

-module(yggdrasil_app).

-behaviour(application).

-export([start/2, stop/1]).
-compile(export_all).

start(_StartType, _StartArgs) ->
    yggdrasil_sup:start_link().

stop(_State) ->
    ok.

%% internal functions

get_tasks() ->
    Method = get,

    % URL = <<"https+unix:<<///var/run/yggdrasil.sock">>,
    URL = <<"https://my-json-server.typicode.com/Kushal-kothari/yggdrasil/db">>,


    Headers = [],
    Payload = <<>>,
    Options = [],

    {ok, 200, _RespHeaders, ClientRef} =
        hackney:request(Method, URL, Headers, Payload, Options),
    {ok, Body} = hackney:body(ClientRef),
    Map = jsx:decode(Body, [return_maps]),
    _Tasks = maps:get(<<"peers">>, Map).

create_table(TableName, Tuples) ->
    ets:new(TableName, [set, named_table]),
    insert(TableName, Tuples).

insert(_Table, []) ->
    ok;
insert(Table, [Tuple|Tuples]) ->
    #{<<"id">> := Id} = Tuple,
    ets:insert(Table, {Id, Tuple}),
    insert(Table, Tuples).

retrieve_task(TableName, Id) ->
    [{_Id, Task}] = ets:lookup(TableName, Id), 
    Task.






%=== MACROS ====================================================================

% -define(BASE_URL, <<"http+unix:///var/run/yggdrasil.sock">>).



%=== API FUNCTIONS =============================================================

% start() ->
%     {ok, _} = application:ensure_all_started(hackney),
%     ok.


% yggdrasil_put(Path, Query, Body) -> yggdrasil_put(Path, Query, Body, #{}).

% yggdrasil_put(Path, Query, Body, Opts) ->
%     ResultType = maps:get(result_type, Opts, json),
%     case hackney:request(put, url(Path, Query), [], Body, []) of
%         {error, _Reason} = Error -> Error;
%         {ok, Status, _RespHeaders, ClientRef} ->
%             case yggdrasil_fetch_json_body(ClientRef, ResultType) of
%                 {error, _Reason} = Error -> Error;
%                 {ok, Response} -> {ok, Status, Response}
%             end
%     end.

% yggdrasil_fetch_json_body(ClientRef, Type) ->
%     case hackney:body(ClientRef) of
%         {error, _Reason} = Error -> Error;
%         {ok, Body} -> decode(Body, Type)
%     end.
